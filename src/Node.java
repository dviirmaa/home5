import java.util.*;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    Node(String n, Node d, Node r) {
        name = n;
        firstChild = d;
        nextSibling = r;

    }
    Node(){

    }

    public static Node parsePostfix(String s) {
        char[] nodes = isInputCorrect(s).toCharArray();

        int rightBracket = 0;
        int leftBracket = 0;


        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] == ')') {
                rightBracket++;
            } else if (nodes[i] == '(') {
                leftBracket++;
            }
        }

            //if (nodes.length == 1 && !(nodes[0] == ',') && !(nodes[0] == '(') || !(nodes[0] == ')')) {
            // throw new RuntimeException("You entered only one root");
            if ((nodes[nodes.length-1] == ',') || (nodes[nodes.length-1] == '(') || (nodes[nodes.length-1] == ',')) {
                throw new RuntimeException("Root is missing, check your input " + s);
            } else if (!(rightBracket == leftBracket)) {
                throw new RuntimeException("The number of right and left brackets mast be equal " + s);
            } else if (checkConsecutiveCommas(nodes)) {
                throw new RuntimeException("Too many commas beside " + s);
            } else if (checkConsecutiveBrackets(nodes)){
                throw new RuntimeException("Too many consecutiva brackets " + s);
            }

            // meetodi koostamisel on kasutatud mõtteid siit: https://git.wut.ee/i231/home5/src/master/src/Node.java
            LinkedList<Node> children = new LinkedList<Node>();
            Node node = new Node();

            for (char ch : nodes) {
                if (ch == '(') {
                    if (!children.isEmpty() && !(node.name == null)) {
                        throw new RuntimeException("Check your input, no parent found " + s);
                    }
                node.firstChild = new Node();
                    children.push(node);
                    node = node.firstChild;
                } else if (ch == ')'){
                    node = children.pop();
                } else if (ch == ','){
                    if (children.isEmpty()){
                        throw new RuntimeException("Check your input, no parent found " + s);
                    }
                    node.nextSibling = new Node();
                    node = node.nextSibling;
                } else {
                    if (node.name == null){
                        node.name = String.valueOf(ch);
                    } else {
                        node.name += ch;
                    }
                }
            }
        return node;
    }

    private static boolean checkConsecutiveBrackets(char[] input) {
        int consecutiveOpeningBrackets = 0;
        int consecutiveClosingBrackets = 0;
        int consecutiveOpeningBracketsTotal;

        for (int j = 0; j < input.length; j++) {
            if (input[j] == '(') {
                consecutiveOpeningBrackets++;
            }else if (!(input[j] == '(')){
                if (consecutiveOpeningBrackets>1){
                    consecutiveOpeningBracketsTotal = consecutiveOpeningBrackets;
                    for (int k = j; k < input.length;k++ ){
                        if(input[k] == ')' ) {
                            consecutiveClosingBrackets++;
                        }else if( !(input[k] == ')') && consecutiveClosingBrackets<2){
                            consecutiveClosingBrackets = 0;
                        }else if (!(input[k] == ')') && consecutiveClosingBrackets==consecutiveOpeningBracketsTotal){
                            return true;
                        }
                    }
                } else {
                    consecutiveOpeningBrackets=0;
                }
            }
        }
        return false;
    }

    // meetodi koostamisel on kasutatud mõtteid siit: https://git.wut.ee/i231/home5/src/master/src/Node.java
    public String leftParentheticRepresentation() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.name);
        if (this.firstChild != null) {
            stringBuilder.append("(" + this.firstChild.leftParentheticRepresentation() + ")");
        }
        if (this.nextSibling != null) {
            stringBuilder.append(",");
            stringBuilder.append(this.nextSibling.leftParentheticRepresentation());
        }

        return stringBuilder.toString();
    }

    private static String isInputCorrect(String input) {

        if (input.contains(" ")){
            throw new RuntimeException("Input contains spaces "+input);
        }
        input = input.replaceAll("\\s+", "");
        if (input.contains(",,")) {
            //System.out.println("Too many commas at the same place");
            throw new RuntimeException("Too many commas in the same place");
        } else if (input.contains("()")) {
            throw new RuntimeException("Missing node inside the bracket");
        } else if (input.contains("(,") || (input.contains("),") || (input.contains(",)")))) {
            throw new RuntimeException("Commas and brackets can't be together side by side");
        }
        return input;
    }

    private static boolean checkConsecutiveCommas(char[] input) {
        int consecutiveCommas = 0;
        //while (consecutiveCommas == 2) {
            for (int j = 0; j < input.length; j++) {
                if (input[j] == ',') {
                    consecutiveCommas++;
                } else {
                    consecutiveCommas = 0;
                }

                if (consecutiveCommas ==2){
                    return true;
                }

            }

        //}
        return false;
    }

    private static boolean checkConsecutiveRoot(char[] input) {
        int consecutiveRoot = 0;
        while (consecutiveRoot == 2) {
            for (int j = 0; j < input.length; j++) {
                if (!(input[j] == ',') || !(input[j] == ')') || !(input[j] == '(')) {
                    consecutiveRoot++;
                } else {
                    consecutiveRoot = 0;
                }
            }
        }
        return true;
    }

    public static void main(String[] param) {
        parsePostfix("A B");
        //String s = "(B1,C)A";
       // Node t = Node.parsePostfix(s);
       // String v = t.leftParentheticRepresentation();
        //System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)
    }
}